import urllib2
import urlparse
import httplib
import os
import sys
import sqlite3

dstdir = 'f:\ipas_j_c'
dbpath = 'e:\download.db'
def is_app_downloaded(url_path):
    print ('is_app_downloaded')
    conn = sqlite3.connect(dbpath)
    c = conn.cursor()
    c.execute('''create table if not exists downloads(url text primary key, local text)''')
    conn.commit()
    conn.close()

    conn = sqlite3.connect(dbpath)
    c = conn.cursor()
    sql = 'select * from downloads where url="%s"'%url_path
    c.execute(sql)
    result = c.fetchall()
    for i in result:
        conn.commit()
        conn.close()
        print i[1]
        return True, i[1]

    conn.commit()
    conn.close()
    return False, ''

def set_app_downloaded(url_path, local_path):
    try:
        print('set_app_downloaded start')
        conn = sqlite3.connect(dbpath)
        c = conn.cursor()
        c.execute('insert into downloads values(?, ?)', (url_path, local_path))
        conn.commit()
        conn.close()
        print('set_app_downloaded end')
    except:
        pass
    
def getsubdstdir():
    subdir = 1
    while True:
        dstsubdir = "%s%s%d%s"%(dstdir, os.sep, subdir, os.sep)
        if not os.path.exists(dstsubdir):
            os.mkdir(dstsubdir)
        dstfiles = os.listdir(dstsubdir)
        if len(dstfiles) >= 10000:
            subdir = subdir + 1
        else:
            return dstsubdir
        
def getfilenamebyurl(url_path):
    firstpos = url_path.rfind('/')
    filename = url_path[firstpos + 1:]
    return filename

def download(url_path):
    try:
        if url_path.endswith('\n'):
            url_path = url_path[0:len(url_path)-1]
        ipaname = getfilenamebyurl(url_path)
        if ipaname == '':
            print('download % happen exception getfilenamebyurl return empty'%url_path)
            return False, ''
        retvalue = is_app_downloaded(url_path)
        if retvalue[0]:
            return True, retvalue[1]
        dstfilepath = getsubdstdir() + ipaname
        if os.path.exists(dstfilepath):
            set_app_downloaded(url_path, dstfilepath)
            return True, dstfilepath
        req = urllib2.Request(url_path)
        resp = urllib2.urlopen(req, timeout=10)
        content = resp.read()
        resp.close()
        tempdstfilepath = dstfilepath + '.tmp'
        f = open(tempdstfilepath, 'wb')
        f.write(content)
        f.close()
        set_app_downloaded(url_path, dstfilepath)
        os.rename(tempdstfilepath, dstfilepath)
        return True, dstfilepath
    except:
        print('donwload %s happen exception'%url_path)
        return False, ''

def load_all_urls():
    url_file_path = 'download_log.log'
    file_input = open(url_file_path, 'r')
    while True:
        record = file_input.readline()
        record = record[0: len(record)-1]
        if (record == ''):
            break
        download(record)
    file_input.close()


if __name__ == "__main__":
    if not os.path.exists(dstdir):
        os.mkdir(dstdir)
    load_all_urls()
