# -*- coding:utf-8 -*-
import ctypes
import threading
import MySQLdb
import os
import socket
import json
import httplib
import urllib2
import uuid
import zipfile
import base64
import sys
import hashlib
import subprocess
import time
import datetime
import xml.dom.minidom
from biplist import *

def commom_log(filename, content , write_type='wb'):
    try:
        f = open(filename , write_type)
        f.write(content);
        f.close()
    except:
        print('happen an exception when write file')
        return ""

def get_appid(url):
    index = url.rfind('id')
    index_right = url.find('?', index)
    if(index_right == -1):
        appid = url[index + 2: len(url)]
    else:
        appid = url[index + 2: index_right]
    return appid

def load_all_useraccount():
    global all_useraccount
    global db_host
    global db_user
    global db_passwd
    global db_port
    global db_name
    global tb_name
    all_useraccount = list()
    f = open('itunes_account_hot.txt' , 'r')
    try:
        conn = MySQLdb.connect(host=db_host,user=db_user,passwd=db_passwd,port=db_port,init_command="set names utf8")
        conn.select_db(db_name)
        #关闭连接，释放资源   
        while True:
            record = f.readline()
            if(record == ''):
                break
            tmp_account = record.strip('\n')

            strlist    = tmp_account.split('|')
            strlist_1  = strlist[0]

            cur  = conn.cursor()
            sql  = 'select count(*) totalcount from '+tb_name+' where useraccount="%s" limit 1;'%(strlist_1)
            cur.execute(sql)
            result = cur.fetchone()
            if result[0] < 800000:
                all_useraccount.append(tmp_account+"|"+str(result[0]))
        cur.close()
        conn.close()
        return all_useraccount
    except MySQLdb.Error, e:
        print "Mysql Error:", e
        sys.exit(0)

def load_all_urls():
    global all_urls
    all_urls = list()
    f = open('itunesurl_hot.txt', 'r')
    while True:
        record = f.readline()
        if (record == ''):
            break
        all_urls.append(record.strip('\n'))
    return all_urls

def buy_app(appid, referUrl, account, password):
    # try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = '127.0.0.1'
    port = 1234
    s.connect((host, port))
    print('connect xigeserver succeed\n')
    data = {'appid': appid, 'referUrl': referUrl, 'appleId': account,'password': password}
    str_data = json.dumps(data)
    s.send(str_data + '\r\n\r\n')
    buf = s.recv(4096*10)
    s.close()
    decodejson = json.loads(buf)
    print "bundleVersion:==="+decodejson['szVersion']+"----shortBundleVersion++:"+decodejson['szShortVersion']+"\n"
    return decodejson['result'], decodejson['szUrl'], decodejson['szKey'], decodejson['szSinf'], decodejson['szArtworkUrl'], decodejson['szMetadataPath'], decodejson['szVersion'], decodejson['szShortVersion'], decodejson['szBundleId']
    # except Exception, e:
    #     return False, '', '', '', '', '', '', '', ''

def download_app(url, key, appid):
    global download_thread_num
    try:
        
        download_thread_num = download_thread_num + 1
        print str(download_thread_num) + ' download nums'
        
        cookie = 'downloadKey=%s'%key
        req = urllib2.Request(url)
        req.add_header('Cookie', cookie)
        req.add_header('User-Agent', 'Mozilla/4.0 (comatible; MSIE 8.0; Win32)')
        req.add_header('Connection', 'Keep-Alive')
        req.add_header('Cache-Control', 'no-cache')
        resp = urllib2.urlopen(req)
        content = resp.read()
        resp.close()
        if not os.path.exists('ipas'):
            os.mkdir('ipas')

        m = hashlib.md5(str(appid))
        m.digest()
        md5  = m.hexdigest()
        str1 = md5[0: 2]
        str2 = md5[2: 4]
        if not os.path.exists('ipas/'+str1):
            os.mkdir('ipas/'+str1)
        if not os.path.exists('ipas/'+str1+"/"+str2):
            os.mkdir('ipas/'+str1+"/"+str2)
        random_filename = 'ipas/'+ str1 + "/" + str2 + "/" + str(md5) + str2 + str1 + '.ipa'
        # print(random_filename)
        f = open(random_filename, 'wb')
        f.write(content)
        f.close()
        if(download_thread_num > 1):
            download_thread_num = download_thread_num -1
        return random_filename
    except:
        if(download_thread_num > 1):
            download_thread_num = download_thread_num -1
        print('happen an exception when download app url is %s key is %s'%(url, key))
        return ""

def download_png(url):
    try:
        req = urllib2.Request(url)
        resp = urllib2.urlopen(req)
        content = resp.read()
        resp.close()
        return True, content
    except:
        print('happen an exception when download png the url is %s'%url)
        return False, ""

def make_newipa(ipaname, png_content, sinf_content, appid, accounthash):
    try:
        zfobj = zipfile.ZipFile(ipaname)
        for name in zfobj.namelist():
            name = name.replace('\\', '/')
            if name.find('SC_Info/') > 0 and not name.endswith('SC_Info/'):
                basename = os.path.basename(name)
                break
        dotindex = basename.find('.')
        newsinfname = basename[0:dotindex] + '.sinf'
        newsinfpath = os.path.dirname(name) + '/' + newsinfname
        zfobj.close()

        # print('newsinfpath is %s'%newsinfpath)
        zf = zipfile.ZipFile(ipaname, "a")

        m = hashlib.md5(appid)
        m.digest()
        md5 = m.hexdigest()
        str1 = md5[0: 2]
        str2 = md5[2: 4]

        a_m = hashlib.md5(accounthash)
        a_m.digest()
        a_md5 = a_m.hexdigest()

        filename  = 'ipas/' + str1 +  '/' + str2 + '/' + str(uuid.uuid4())

        tempFile = open(filename, 'wb')
        base64_content = base64.decodestring(sinf_content)
        tempFile.write(base64_content)
        tempFile.close() 
        zf.write(filename, newsinfpath)

        tempFile = open(filename, 'wb')
        tempFile.write(png_content)
        tempFile.close() 
        zf.write(filename, 'iTunesArtwork')
        os.remove(filename)

        plist_name = 'c:/iTunes/'+str(appid)+'.plist'
        if not os.path.exists(plist_name):
            return False
        else:
            fplist = open(plist_name , 'r')
            meta_path = ''
            while 1:
                line = fplist.readline()
                if not line:
                    break
                else:
                    meta_path += line
            fplist.close()
            os.remove(plist_name)

            tempFile = open(filename, 'wb')
            meta_path = meta_path.split("</dict>")
            meta_path_new = ''
            count = 0
            for sub_data in meta_path:
                if len(meta_path) - 1 != count:
                    if len(meta_path) - 2 == count :
                        meta_path_new = meta_path_new + sub_data+"<key>account hash</key><string>"+a_md5+"</string></dict></plist>"
                    else:
                        meta_path_new = meta_path_new + sub_data+"</dict>"
                    count = count + 1
            if(meta_path_new == ''):
                meta_path_new = meta_path
            tempFile.write(meta_path_new)
            tempFile.close() 
            zf.write(filename, 'iTunesMetadata.plist')
            os.remove(filename)

            zf.close()
            return True
    except:
        print('when make_newipa happen an error')
        return False

def save_app_info(appid, state, appname, url, cookie, orign_url, bundleVersion, account, bundleId , version):
    global sqlite_mutex
    global db_host
    global db_user
    global db_passwd
    global db_port
    global db_name
    global tb_name
    sqlite_mutex.acquire()
    try:

        conn = MySQLdb.connect(host=db_host,user=db_user,passwd=db_passwd,port=db_port,init_command="set names utf8")
        conn.select_db(db_name)
        cur  = conn.cursor()
        # is_install 表示有 正版安装包
        print "bundleVersion:==="+bundleVersion+"----shortBundleVersion++:"+version+"\n"
        # if version == '':
        #     sql  = 'update '+tb_name+' set status=%d, useraccount="%s",is_install=1,bundleid="%s",bundleVersion="%s" where itunesid="%s"'%(state, account, bundleId,bundleVersion,appid)
        # else:

        # type_val = get_equipmenttype(appname)

        sql  = 'select count(*) totalcount from '+tb_name+' where itunesid="%s" and version="%s" and bundleVersion="%s" limit 1;'%(appid,version,bundleVersion)
        cur.execute(sql)
        print (sql)
        result = cur.fetchone()
        print "have new ipa "+str(result[0]) + " , 1 NO , 0 YES \n"
        if result[0] == 0:
            # sql  = 'update '+tb_name+' set status=%d, useraccount="%s",is_install=1,bundleid="%s",version="%s",bundleVersion="%s",equipmenttype=%d where itunesid="%s"'%(state, account, bundleId,version,bundleVersion,type_val,appid)
            sql  = 'update '+tb_name+' set status=%d, useraccount="%s",is_install=1,bundleid="%s",version="%s",bundleVersion="%s" where itunesid="%s"'%(state, account, bundleId,version,bundleVersion,appid)
        else:
            # sql  = 'update '+tb_name+' set status=0, useraccount="%s",is_install=1,bundleid="%s",equipmenttype=%d where itunesid="%s"'%(account, bundleId,type_val,appid)
            sql  = 'update '+tb_name+' set status=0, useraccount="%s",is_install=1,bundleid="%s" where itunesid="%s"'%(account, bundleId,appid)

        print(sql)
        cur.execute(sql)
        conn.commit()
        cur.close()
        conn.close()
    except MySQLdb.Error, e:
        print "Mysql Error:", e
    sqlite_mutex.release()


def get_itunes_info(appid):
    global sqlite_mutex
    global db_host
    global db_user
    global db_passwd
    global db_port
    global db_name
    global tb_name
    result = []
    sqlite_mutex.acquire()
    try:
        conn = MySQLdb.connect(host=db_host,user=db_user,passwd=db_passwd,port=db_port,init_command="set names utf8")
        conn.select_db(db_name)
        cur  = conn.cursor()
        sql  = 'select itunesid,version,bundleVersion,status from '+tb_name+' where itunesid="%s" limit 1;'%(appid)
        cur.execute(sql)
        result = cur.fetchone()
        cur.close()
        conn.close()
    except MySQLdb.Error, e:
        print "Mysql Error:", e
    sqlite_mutex.release()

    return result

def del_buy_free_appp(appid):
    global db_host
    global db_user
    global db_passwd
    global db_port
    global db_name
    global tb_name
    try:
        conn = MySQLdb.connect(host=db_host,user=db_user,passwd=db_passwd,port=db_port,init_command="set names utf8")
        conn.select_db(db_name)
        cur  = conn.cursor()
        sql  = 'delete from '+tb_name+' where itunesid="%s" limit 1;'%(appid)
        print(sql)
        cur.execute(sql)
        conn.commit()
        #关闭连接，释放资源   
        cur.close()
        conn.close()
    except MySQLdb.Error, e:
        print "Mysql Error:", e

def load_all_urls_DB():
    global db_host
    global db_user
    global db_passwd
    global db_port
    global db_name
    global tb_name
    if os.path.isfile('itunesurl_hot.txt'):
        os.remove('itunesurl_hot.txt')

    try:
        conn = MySQLdb.connect(host=db_host,user=db_user,passwd=db_passwd,port=db_port,init_command="set names utf8")
        conn.select_db(db_name)
        cur  = conn.cursor()
        # status 2 ： 新增 3 ： 更新
        sql  = "select itunesid,itunes,status from "+tb_name+" where (status=2 or status = 3) and isopen=2 and itunesid not in(590193710) ;";
        #sql  = "select itunesid,itunes from "+tb_name+" where itunesid = '737440896';";
        cur.execute(sql)
        result = cur.fetchall()

        fp = open('itunesurl_hot.txt', 'wb')

        for f in result:
            fp.write(f[1] + "\n")

        conn.commit()
        #关闭连接，释放资源   
        cur.close()
        conn.close()
    except MySQLdb.Error, e:
        print "Mysql Error:", e

def check_plist_exists(random_filename):
    try:
        zf = zipfile.ZipFile(random_filename, "a")
        zf.getinfo("iTunesMetadata.plist")
        data = zf.read("iTunesMetadata.plist")
        if data :
            return 1
        else:
            return 2
    except:
        return 3

def get_equipmenttype(random_filename):
    zf       = zipfile.ZipFile(random_filename, "r")
    nList    = zf.namelist()
    type_val = 1

    for f in nList:
        if f.rfind('Info.plist') != -1:
            # print f + "\n"
            f_arr = f.split("/")
            if len(f_arr) == 3:
                if(f_arr[2] == 'Info.plist'):
                    # print f + "\n"
                    info  = zf.read(f)
                    plist = readPlistFromString(info)
                    # print plist
                    print plist['UIDeviceFamily']
                    if len(plist['UIDeviceFamily']) == 1:
                        type_val = plist['UIDeviceFamily'][0]
                    elif len(plist['UIDeviceFamily']) > 1:
                        type_val = 3
    return type_val


def thread_main(arg):
    global all_urls
    global all_account_cur
    global all_password_cur
    global all_account_cur_num  # 记录当前帐号 总的请求次数
    global return_error_nums

    while True:
        #加入线程锁
        threadname = threading.currentThread().getName()
        urls_mutex.acquire()
        if len(all_urls):
            url = all_urls.pop(0)
            appid = get_appid(url)
        else:
            urls_mutex.release()
            break
        if(all_account_cur==""):
            if len(all_useraccount):
                account_cur = all_useraccount.pop(0)
                strlist = account_cur.split('|')
                all_account_cur  = strlist[0]
                all_password_cur = strlist[1]
                all_account_cur_num = int(strlist[2])
            else:
                urls_mutex.release()
                break
        urls_mutex.release()
        print('current account : ' + all_account_cur +" ---  threadname: " + threadname +"\n")
        retvalue = buy_app(appid, url, all_account_cur, all_password_cur)
        datetime_now = datetime.datetime.now()
        datetime_now = datetime_now.strftime("%Y-%m-%d %H:%M")
        # print retvalue
        print('buyapp ret 1'+ " : " + datetime_now + " == " + str(all_account_cur_num))
        #  `status` '0未购买 1购买成功 2ipa下载成功 3购买失败 4ipa下载失败 5 下载png失败 6 make_newipa ipa成功（这是才是最终的完成） 7make_newipa失败',
        # 返回值 1 成功 ，2004 帐号被禁用
        print str(retvalue[0]) + "===" + str(appid)
        if retvalue[0] == '1':

            old_appInfo = get_itunes_info(appid)
            is_download = 0
            if old_appInfo:
                print "test_version: "+str(old_appInfo[1]) +"==="+ str(retvalue[7]) + "===" + str(old_appInfo[2]) +"==="+ str(retvalue[6])
                if old_appInfo[1] == retvalue[7] and old_appInfo[2] == retvalue[6]:
                    is_download = 0
                    save_app_info(appid, 6, "", retvalue[1], retvalue[2], url, retvalue[6], all_account_cur, retvalue[8] , retvalue[7])
                else:
                    is_download = 1
            else:
                is_download = 1

            if is_download == 1:
                all_account_cur_num = all_account_cur_num + 1
                print('buy app succeed now download app')
                ipaname = download_app(retvalue[1], retvalue[2], appid)
                print('download app over')
                if ipaname == '':
                    if retvalue[2] != '':# 已经购买过的ipas不能再次购买需要用其它的帐号购买
                        # save_app_info(appid, 4, '', retvalue[1], retvalue[2], url, retvalue[6], all_account_cur)
                        all_urls.append(url)
                        continue
                    else:
                        print "==return download key is null==buy already=====";
                        #del_buy_free_appp(appid)
                        continue
                (result,png_content) = download_png(retvalue[4])
                if not result:
                    # save_app_info(appid, 5, '', retvalue[1], retvalue[2], url, retvalue[6], all_account_cur)
                    all_urls.append(url)
                    continue
                if make_newipa(ipaname, png_content, retvalue[3], appid, all_account_cur):
                    # retvalue[6] 表示 bundleVersion ，retvalue[7] 表示 shortBundleVersion ， retvalue[8] 表示 bundleid
                    plist_value = check_plist_exists(ipaname)
                    if plist_value == 1:
                        print str(appid)+"bundleVersion:==="+retvalue[6]+"----shortBundleVersion++:"+retvalue[7]+"\n"
                        save_app_info(appid, 6, ipaname, retvalue[1], retvalue[2], url, retvalue[6], all_account_cur, retvalue[8] , retvalue[7])
                    else:
                        commom_log('plist_error.log' , str(appid) + "|" + ipaname + "|" + str(plist_value) + "\n")
                else:
                    # save_app_info(appid, 7, '', retvalue[1], retvalue[2], url, retvalue[6], all_account_cur)
                    all_urls.append(url)
            else:
                print "no app update : " + str(appid)
        elif retvalue[0]== '2004':
            all_urls.append(url) # 从新加回队列 继续下载 帐号被禁用
            print(all_account_cur+' is disabled'+" ---  threadname: " + threadname +"\n")
            continue
        elif retvalue[0] == '-2':
            all_urls.append(url) # 查询 server 失败
            continue
        elif  retvalue[0] == '-13':#限时免费 开始收费了 删除免费行列
            del_buy_free_appp(appid)
            continue
        elif retvalue[0] == '-14': #商品开始下架
            del_buy_free_appp(appid)
            continue
        elif retvalue[0] == False:
            all_urls.append(url) # 查询 server 失败
            continue
        elif retvalue[0] == '-33':
            sys.exit('return -33')

    print('thread exit')
    
def main():
    global sqlite_mutex
    global urls_mutex
    global all_account_cur
    global all_password_cur
    global all_account_cur_num
    global download_thread_num
    global return_error_nums
    global all_urls

    global db_host
    global db_user
    global db_passwd
    global db_port
    global db_name
    global tb_name


    db_host   = "127.0.0.1"
    db_user   = 'root'
    db_passwd = ''
    db_port   = 3306
    db_name   = "app_xyapplist"
    tb_name   = "t_xyapplist_hot"
    all_urls  = list()

    socket.setdefaulttimeout(6000)

    print('start XiGeServer')
    p_xigeserver_port    = 1234
    root_path = os.getcwd()
    root_path = root_path.replace("\\" , "/")
    p_xigeserver = subprocess.Popen(root_path+"/XiGeServer.exe "+str(p_xigeserver_port))

    while True:
        print('start')
        if len(all_urls) == 0:
            download_thread_num  = 0
            return_error_nums    = 0
            all_account_cur      = ''
            all_password_cur     = ''
            all_account_cur_num  = 0

            load_all_urls_DB()
            load_all_useraccount()
            load_all_urls()
            if len(all_urls) > 0:
                threads = []
                urls_mutex   = threading.Lock()
                sqlite_mutex = threading.Lock()
                for x in xrange(0, 15):
                    threads.append(threading.Thread(target=thread_main, args=(15,)))
                for t in threads:
                    t.start()
                for t in threads:
                    t.join()
            else:
                print("break loop.. wait 10 min")
                time.sleep(60*5)
                continue
            print('end')
        else:
            print("python download now time sleep ..wait 10 min..")
            time.sleep(60*10)


if __name__ == '__main__':
    reload(sys) 
    sys.setdefaultencoding('utf8')
    main()
