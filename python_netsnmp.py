#-*- coding:utf-8 -*-
import os, sys
import socket
import random
# import netsnmp
from struct import pack, unpack
from datetime import datetime as dt

from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.proto.rfc1902 import Integer, IpAddress, OctetString

reload(sys)
sys.setdefaultencoding('utf-8')

ip='192.168.0.51'
community='aruba123'

#from snmp_helper import snmp_get_oid,snmp_extract

# value=(1,3,6,1,2,1,17,7,1,2,2,1,2)
# value = ('1.3.6.1.4.1.14823.2.2.1.2.1.19.1') #ac
#wlsxUserEntry 
# value =   ('1.3.6.1.4.1.14823.2.2.1.4.1.2.1')

#wlsxUserEntry 
# value =   ('1.3.6.1.4.1.14823.2.2.1.5.3.1.1.1')
value  = '1.3.6.1.4.1.14823.2.2.1.2.1';

# value = ('1.3.6.1.4.1.14823.2.2.1.2.1.16.1.6.2')

generator = cmdgen.CommandGenerator()
comm_data = cmdgen.CommunityData('server', community, 1) # 1 means version SNMP v2c
transport = cmdgen.UdpTransportTarget((ip, 161))

# 获取 单条 
# real_fun = getattr(generator, 'getCmd')
# 获取 多条
real_fun = getattr(generator, 'nextCmd')

res = (errorIndication, errorStatus, errorIndex, varBinds)\
    = real_fun(comm_data, transport, value)

if not errorIndication is None  or errorStatus is True:
       print "Error: %s %s %s %s" % res
else:
    for item in varBinds:
        # print item[0] , '=' , item[1]
        print item[0]
       # print "%s" % varBinds