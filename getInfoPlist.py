# -*- coding:utf-8 -*-
import ctypes
import os
import socket
import json
import zipfile
import base64
import sys
import hashlib
import datetime
import time
import subprocess
import xml.dom.minidom
from biplist import *


def commom_log(filename, content , write_type='wb'):
    try:
        f = open(filename , write_type)
        f.write(content);
        f.close()
    except:
        print('happen an exception when write file')
        return ""

def getInfoPlist(random_filename):
    try:
        type_val = 0
        BundleId = ''
        CFBundleShortVersionString = ''
        CFBundleVersion = ''
        f = zipfile.ZipFile(random_filename, 'r')
        for name in f.namelist():
            if name.endswith('.app/Info.plist'):
                data = f.read(name)
                plist = readPlistFromString(data)
                f.close()
                BundleId = plist['CFBundleIdentifier']
                if plist.has_key('CFBundleShortVersionString'):
                    CFBundleShortVersionString = plist['CFBundleShortVersionString']
                else:
                    CFBundleShortVersionString = ''
                if plist.has_key('CFBundleVersion'):
                    CFBundleVersion = plist['CFBundleVersion']
                else:
                    CFBundleVersion = ''
        return BundleId,CFBundleShortVersionString,CFBundleVersion
    except:
        return '','',''

if len(sys.argv) <=1 :
    print "-1"
else:
    random_filename = sys.argv[1]
    if os.path.exists(random_filename):
        BundleId = getInfoPlist(random_filename)
        print BundleId
    else:
        print "-2"