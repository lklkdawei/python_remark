# -*- coding:utf-8 -*-
import ctypes
import threading
import MySQLdb
import os
import socket
import json
import httplib
import urllib2
import uuid
import zipfile
import base64
import sys
import hashlib
import datetime
import time
import subprocess
import xml.dom.minidom

db_host   = "127.0.0.1"
db_user   = 'root'
db_passwd = ''
db_port   = 3306
db_name   = "app_xyapplist"

def commom_log(filename, content , write_type='wb'):
    try:
        f = open(filename , write_type)
        f.write(content);
        f.close()
    except:
        print('happen an exception when write file')
        return ""

def get_version(random_filename):
    try:
        zf = zipfile.ZipFile(random_filename, "a")
        zf.getinfo("iTunesMetadata.plist")
        data = zf.read("iTunesMetadata.plist")
        if data :
            index   = data.rfind('bundleShortVersionString')
            if index != -1:
                index_right = data.find('</string>', index)
                version = data[index: index_right]
                version = version.replace("bundleShortVersionString</key>" , '').replace("<string>", "").strip()
            else:
                version = ''
            index   = data.rfind('bundleVersion')
            if index != -1:
                index_right = data.find('</string>', index)
                bundleVersion = data[index: index_right]
                bundleVersion = bundleVersion.replace("bundleVersion</key>" , '').replace("<string>", "").strip()
            else:
                bundleVersion = ''
            return  version,bundleVersion
        else:
            return '',''    
    except:
        return '',''


conn = MySQLdb.connect(host=db_host,user=db_user,passwd=db_passwd,port=db_port,init_command="set names utf8")
conn.select_db(db_name)
cur  = conn.cursor()
sql  = "select itunesid , version , bundleVersion from t_xyapplist_hot where is_install = 1;";
print sql
cur.execute(sql)
result = cur.fetchall()
for f in result:
    itunesid = f[0]
    db_version = f[1]
    db_bundleVersion = f[2]

    print itunesid
    m = hashlib.md5(str(itunesid)) 
    m.digest()
    md5  = m.hexdigest()
    str1 = md5[0: 2]
    str2 = md5[2: 4]

    random_filename = 'F:/app_up/ipas/'+ str1 + "/" + str2 + "/" + str(md5) + str2 + str1 + '.ipa'
    # random_filename = 'D:/work/python/ipas/00/2f/002f3b54abb48e7398010c8fc382f9fc2f00.ipa'
    if os.path.exists(random_filename):
        return_value = get_version(random_filename)
        if return_value[0] != '' :
            if return_value[1] != '':
                update_sql  = "update t_xyapplist_hot set version='"+str(return_value[0])+"' ,bundleVersion='"+str(return_value[1])+"' where itunesid = '"+str(itunesid)+"' limit 1;";
                print random_filename+"\n"
                print update_sql+"\n"
                commom_log('update_version_sql.log' , update_sql + "==|||==" + db_version + "\n" , 'a')
                cur.execute(update_sql)

cur.close()
conn.close()